MindStalker

Kivy Python based display/recording/replay software for brain wave device

The basic work tool is a hacked EEG Toy named MindFlex, an Arduino Nano module, and a Raspberry Pi. The software is written in Python, using Kivy to create a touchscreen graphical interface. The Arduino Board sends data from the TGAM1 board. It sends 9600 bits per second every second (this is a low rate , which creates a future attempt to replace the toy with a better reading device) that includes 8 EEG power bands. The Raspberry Pi has Raspbian distribution installed, while showing the GUI of MindStalker on its touchscreen.

The recorded data are saved as a NumPy array and can be played back at any time. The option to send the data to other applications via OSC opens the gate to use it artistically. OSC can be used to make music, create visualizations, or control mechanical device such as motors or LED lights.
