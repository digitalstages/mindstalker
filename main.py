import os
# Tell the RPi to use the TFT screen and that it's a touchscreen device
#os.environ['KIVY_TEXT'] = 'pil'
#os.putenv('SDL_VIDEODRIVER', 'aalib')
#os.putenv('SDL_FBDEV'      , '/dev/fb0')
# os.putenv('SDL_MOUSEDRV'   , 'TSLIB')
# os.putenv('SDL_MOUSEDEV'   , '/dev/input/mice')

import serial
import csv
from serial import SerialException
import sys
import time
import threading, logging
import numpy as np
import argparse

from pathlib import Path
from kivy.properties import ListProperty, NumericProperty, OptionProperty, ObjectProperty
from kivy.uix.togglebutton import ToggleButton
from kivy.uix.popup import Popup
from kivy.uix.button import Button
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
from kivy.app import App
from kivy.clock import Clock
from kivy.factory import Factory
from viewport import Viewport

from pythonosc import osc_message_builder
from pythonosc import udp_client

from distutils.util import strtobool


class LoadDialog(FloatLayout):    
    load = ObjectProperty(None)
    cancel = ObjectProperty(None)
    

class SaveDialog(FloatLayout):
    save = ObjectProperty(None)
    cancel = ObjectProperty(None)

    
class MySettings(FloatLayout):
    
    _dev_path = ObjectProperty(None)
    _asave_bool = ObjectProperty(None)
    _asave_time = ObjectProperty(None)
    _osc_bool = ObjectProperty(None)
    _osc_ip = ObjectProperty(None)
    _osc_port = ObjectProperty(None)
    _sc_bool = ObjectProperty(None)
    _jack_bool = ObjectProperty(None)
    _sc_path = ObjectProperty(None)
    apply = ObjectProperty(None)
    cancel = ObjectProperty(None)
    
    
class ExitMenu(BoxLayout):
    cancel = ObjectProperty(None)
                
    def desktop(self):
        App.get_running_app().stop()
        print("Exit Program")
    
    def shutdown(self):
        os.system('sudo halt')
        
class MindStalkerGrid(GridLayout):
    
    loadfile = ObjectProperty(None)
    savefile = ObjectProperty(None)
    text_input = ObjectProperty(None)
    
    autosave = np.array([])
    
    pressed = 0
    bool_osc = False
    bool_connect = False
    bool_record = False
    bool_play = False
    bool_stop = False
    
    values_display = 25
    values_delta = ListProperty([])
    values_theta = ListProperty([])
    values_alphaL = ListProperty([])
    values_alphaH = ListProperty([])
    values_betaL = ListProperty([])
    values_betaH = ListProperty([])
    values_gammaL = ListProperty([])
    values_gammaH = ListProperty([])
    
    play_delta = ListProperty([])
    play_theta = ListProperty([])
    play_alphaL = ListProperty([])
    play_alphaH = ListProperty([])
    play_betaL = ListProperty([])
    play_betaH = ListProperty([])
    play_gammaL = ListProperty([])
    play_gammaH = ListProperty([])
    
    data_array = []
    output = []
    
    rec_autosave = False
    rec_array = []
    asave_trig = False
    asave_count = 0
      
    entries_count = 0
    current_count = 0

#    baudrate = 9600
    baudrate = 57600
    dev_present = True
    #Content of settings dictionary
    dev_path = "/dev/ttyUSB1"
    asave_bool = False
    asave_time = 10
    osc_bool = False
    osc_ip = "127.0.0.1"
    osc_port = 57120
    sc_bool = False
    jack_bool = False
    sc_path = "sc/mindstalker.scd"
    
    settings = {'dev_path': dev_path, 
                'asave_bool': str(asave_bool), 
                'asave_time': str(asave_time), 
                'osc_bool': str(osc_bool), 
                'osc_ip': osc_ip, 
                'osc_port': str(osc_port),
                'sc_bool': str(sc_bool),
                'jack_bool': str(jack_bool),
                'sc_path': sc_path}
    
    #Load settings from csv file if exists
    if os.path.isfile('settings.csv'):
        settings = csv.DictReader(open("settings.csv"))
        for row in settings:
            dev_path = row['dev_path']
            asave_bool = row['asave_bool']
            asave_time = row['asave_time']
            osc_bool = row['osc_bool']
            osc_ip = row['osc_ip']
            osc_port = row['osc_port']
            sc_bool = row['sc_bool']
            jack_bool = row['jack_bool']
            sc_path = row['sc_path']
            asave_bool = strtobool(asave_bool)
            asave_time = int(asave_time)
            osc_bool = strtobool(osc_bool)
            osc_port = int(osc_port)
            sc_bool = strtobool(sc_bool)
            jack_bool = strtobool(jack_bool)
            
        print("SETTINGS.CSV LOADED")

    else:        
        with open('settings.csv', 'w') as csvfile:
            fieldnames = ['dev_path', 'asave_bool', 'asave_time', 'osc_bool', 'osc_ip', 'osc_port', 'sc_bool', 'jack_bool', 'sc_path']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            writer.writerow({'dev_path': dev_path, 
                             'asave_bool': asave_bool, 
                             'asave_time': asave_time,
                             'osc_bool': osc_bool,
                             'osc_ip': osc_ip,
                             'osc_port': osc_port,
                             'sc_bool': sc_bool,
                             'jack_bool': jack_bool,
                             'sc_path': sc_path})
            print("CSV created")
    
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", default=osc_ip, help="The ip of the OSC server")
    parser.add_argument("--port", type=int, default=osc_port, help="The port the OSC server is listening on")
    args = parser.parse_args()

    client = udp_client.SimpleUDPClient(args.ip, args.port)
    
    tc_started = False
    tc_seconds = 0
    tc_count = 0
    
    play_pos = 0
    rec_size = 0
    
    alert_message = "MAYDAY! MAYDAY! U CUT THE JUICE!"
    
    
    logging.basicConfig(level=logging.DEBUG,
                        format='[%(levelname)s] (%(threadName)-10s) %(message)s',
                        )
    
    def dismiss_popup(self):
        self._popup.dismiss()

        
    def show_load(self):
        content = LoadDialog(load=self.load, cancel=self.dismiss_popup)
        self._popup = Popup(title="LOAD FILE", content=content,
                            size_hint=(1, 1))
        self._popup.open()
        

    def show_save(self):
        content = SaveDialog(save=self.save, cancel=self.dismiss_popup)
        self._popup = Popup(title="SAVE FILE", content=content,
                            size_hint=(1, 1))
        self._popup.open()
        
        
    def show_settings(self):
        content = MySettings(apply=self.apply, 
                             cancel=self.dismiss_popup, 
                             _dev_path = self.dev_path,
                             _asave_bool = self.asave_bool,
                             _asave_time = self.asave_time,
                             _osc_bool = self.osc_bool,
                             _osc_ip = self.osc_ip,
                             _osc_port = self.osc_port,
                             _sc_bool = self.sc_bool,
                             _jack_bool = self.jack_bool,
                             _sc_path = self.sc_path)
        self._popup = Popup(title="SETTINGS", content=content,
                            size_hint=(1, 1))
        self._popup.open()
        
                
    def alert_popup(self, text): 
        popup = Popup(title='ALERT MESSAGE',
                      content=Label(text=text, font_size=20),
                      size_hint=(None, None), size=(400, 250))
        popup.open()
        
                    
    def exit(self):
        # create content and add to the popup
        content = ExitMenu(cancel=self.dismiss_popup)
        self._popup = Popup(title="EXIT MENU", content=content, size_hint=(None,None), size=(400, 250))
        self._popup.open()
        
         
    def apply(self, _dev_path, _asave_bool, _asave_time, _osc_bool, _osc_ip, _osc_port, _sc_bool, _jack_bool, _sc_path):
        
        if _dev_path:
            self.dev_path = _dev_path
            self.init_eeg()
            
            
        if _asave_bool:
            self.asave_bool = _asave_bool
            if _asave_time:
                self.asave_time = _asave_time
                

        if _osc_bool:
            self.osc_bool = _osc_bool
            self.osc_ip = _osc_ip
            self.osc_port = _osc_port
            #Get OSC client ready
            self.parser = argparse.ArgumentParser()
            self.parser.add_argument("--ip", default=self.osc_ip, help="The ip of the OSC server")
            self.parser.add_argument("--port", type=int, default=self.osc_port, help="The port the OSC server is listening on")
            args = self.parser.parse_args()
            #Start client with set ip and port
            self.client = udp_client.SimpleUDPClient(args.ip, args.port)
            

        if _sc_bool:
            self.sc_bool = _sc_bool
            self.jack_bool = _jack_bool
            if _jack_bool:
                #Start Jackserver
                os.system('jackd -P75 -dalsa -dhw:0 -p512 -n3 -s -r44100 &')
            else:
                #Stop Jackserver
                os.system('pkill jackd && pkill jackd')
                
            self.sc_path = os.path.abspath(self.sc_path)
            
            #Start Supercollider file
            print(self.sc_path)
            sc_file = Path(self.sc_path)
            
            if sc_file.is_file():
                os.system('sclang -D ' + (self.sc_path))
            
            else:
                self.sc_bool = False
                self.ids.sc_bool.active = self.sc_bool
                self.alert_message = "NO SCD FILE DETECTED!\n PLEASE ENTER CORRECT PATH \nAND file.scd."
                self.alert_popup(self.alert_message)
        
        print("I am here")
        self.save_settings()
        print("SETTINGS SAVED")
        #initialize eeg device
        self.init_eeg()
        
    
    def save_settings(self):
        with open('settings.csv', 'w') as csvfile:
            fieldnames = ['dev_path', 'asave_bool', 'asave_time', 'osc_bool', 'osc_ip', 'osc_port', 'sc_bool', 'jack_bool', 'sc_path']
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            writer.writerow({'dev_path': self.dev_path,
                             'asave_bool': self.asave_bool, 
                             'asave_time': self.asave_time,
                             'osc_bool': self.osc_bool,
                             'osc_ip': self.osc_ip,
                             'osc_port': self.osc_port,
                             'sc_bool': self.sc_bool,
                             'jack_bool': self.jack_bool,
                             'sc_path': self.sc_path})
            print("SETTINGS.CSV UPDATED")
            
    
    def load(self, path, filename):
        print(filename)
        if filename:
            if self.bool_connect == True:
                self.ids.toggle_connect.state = "normal"
                self.close_egg()
                Clock.unschedule(self.updateClock)
                self.current_count = 0
                self.output[:]=[]
                self.ids.connect_info.text = "DISCONNECTED" 
                self.bool_connect = False
            
            #Load array from file into wave arrays
            self.rec_array = []
            self.rec_array = np.load(os.path.join(filename[0])).tolist()
            #fill value arrays with data from rec_array
            
            for i in range(len(self.rec_array)):
                self.output = self.rec_array[i]
                self.play_delta.append(self.output[3])
                self.play_theta.append(self.output[4])
                self.play_alphaL.append(self.output[5])
                self.play_alphaH.append(self.output[6])
                self.play_betaL.append(self.output[7])
                self.play_betaH.append(self.output[8])
                self.play_gammaL.append(self.output[9])
                self.play_gammaH.append(self.output[10])
                
            #Disable button while having play function, enable save button
            self.ids.toggle_connect.disabled = True
            self.ids.press_save.disabled = True
            self.ids.toggle_record_play.disabled = False
            
            #change text color of REC button
            self.ids.toggle_record_play.color = (1,1,1,1)
            self.ids.toggle_record_play.text = 'PLAY'
            
            #set play position to 0 and set size of recorded sequence
            self.play_pos = 0
            self.tc_start = self.play_pos
            self.tc_count = 0
            self.current_count = 0
            self.ids.tc_slider.max = len(self.rec_array)
            self.ids.tc_slider.steps = len(self.rec_array)
            self.rec_size = int(str(len(self.rec_array)))
            self.bool_stop = True
            self.bool_record = False
            self.bool_play = False
            print("DATA LOADED")
        else:
            print("NO DATA LOADED")
        #close popup window
        self.dismiss_popup()
        

    def save(self, path, filename):
        if len(self.data_array) > 0:
            #convert list into numpy array and autosave
            np.save(os.path.join(path, filename), np.array(self.rec_array))
        else:
            print("NO DATA TO SAVE. PLEASE RECORD A SESSION FIRST!")
        #close popup window
        self.dismiss_popup()
    
    
    def init_eeg(self):
        try:
            self.ser = serial.Serial()
            self.ser.baudrate = self.baudrate
            self.ser.port = self.dev_path
            print ("# using hard coded defaults " + self.dev_path + " " + str(self.baudrate))
            self.dev_present = True
            self.ser.open()
        except serial.SerialException:
            print("PLEASE CONNECT YOUR DEVICE OR CHANGE ITS ADDRESS")
            self.alert_message = "NO EEG DEVICE DETECTED!\n PLEASE CHECK USB CONNECTION \nOR DEVICE ADDRESS"
            self.alert_popup(self.alert_message)
            self.dev_present = False
    
    
    def read_eeg(self):
        self.flag = 1
        while self.flag and self.ser.is_open:
            try:
                self.input = self.ser.readline()                
                if len(self.input) > 2:                   
                    self.output = [int(i) for i in self.input.decode().split(',')]
                    print(self.output)
                    self.data_array.append(self.output)
                    self.entries_count += 1                    
            except:
                self.flag = 0
                self.entries_count = 0


    def close_egg(self):
        self.ser.close()

        
    def press_all(self):
        if self.pressed == 0:
            self.ids.delta.state = "normal"
            self.ids.theta.state = "normal"
            self.ids.alpha_low.state = "normal"
            self.ids.alpha_high.state = "normal"
            self.ids.beta_low.state = "normal"
            self.ids.beta_high.state = "normal"
            self.ids.gamma_low.state = "normal"
            self.ids.gamma_high.state = "normal"
            self.pressed = 1
        elif self.pressed == 1:
            self.ids.delta.state = "down"
            self.ids.theta.state = "down"
            self.ids.alpha_low.state = "down"
            self.ids.alpha_high.state = "down"
            self.ids.beta_low.state = "down"
            self.ids.beta_high.state = "down"
            self.ids.gamma_low.state = "down"
            self.ids.gamma_high.state = "down"
            self.pressed = 0
        
            
    def press_connect(self):
        if self.bool_connect == False:
            print('Brainwaves On')
            self.init_eeg()
            if self.dev_present:
                self.eeg_process = threading.Thread(target=self.read_eeg)
                self.eeg_process.setDaemon(True)
                self.eeg_process.start()
                Clock.schedule_interval(self.update_values, 0.5)
                self.ids.connect_info.text = "CONNECTED"
                self.ids.toggle_record_play.disabled = False
                self.bool_connect = True
                self.ids.toggle_connect.color = (0,1,0,1)
            
                
        elif self.bool_connect == True:
            print('Brainwaves Off')
            self.bool_connect = False
            if self.ser.is_open:
                self.close_egg()
                Clock.unschedule(self.update_values)
                self.current_count = 0
                self.output[:]=[]
                self.ids.connect_info.text = "DISCONNECTED"
                self.ids.toggle_record_play.disabled = True
                self.ids.toggle_connect.color = (1,1,1,1)
            self.press_new()
            
            
    def press_record_play(self):  
        #bool_record and bool_play
        if self.bool_record == False and self.bool_play == False and self.bool_stop == False:
            
            #toggle record button
            self.tc_start = self.current_count
            self.tc_count = 0
            #start recording into autosave array
            self.rec_autosave = True
            #change text color to red
            self.ids.toggle_record_play.color = (1, 0, 0, 1)
            #activate rec flag            
            self.bool_record = True
                        
        elif self.bool_record == True and self.bool_play == False and self.bool_stop == False:
            #Disconnect EEG
            self.bool_connect = False
            self.ids.toggle_connect.state = "normal"
            self.ids.connect_info.text = "DISCONNECTED" 
            Clock.unschedule(self.update_values)
            self.close_egg()
            #stop recording
            self.rec_autosave = False
            #convert list into numpy array and autosave
            np.save('autosave/autosave', np.array(self.rec_array))
            self.rec_array=[]
            self.play_delta=[]
            self.play_theta=[]
            self.play_alphaL=[]
            self.play_alphaH=[]
            self.play_betaL=[]
            self.play_betaH=[]
            self.play_gammaL=[]
            self.play_gammaH=[]
            #load file from temp file            
            self.rec_array = np.load('autosave/autosave.npy').tolist()
            #fill value arrays with data from rec_array
            for i in range(len(self.rec_array)):
                self.output = self.rec_array[i]
                self.play_delta.append(self.output[3])
                self.play_theta.append(self.output[4])
                self.play_alphaL.append(self.output[5])
                self.play_alphaH.append(self.output[6])
                self.play_betaL.append(self.output[7])
                self.play_betaH.append(self.output[8])
                self.play_gammaL.append(self.output[9])
                self.play_gammaH.append(self.output[10])
                
            #Disable button while having play function, enable save button
            self.ids.toggle_connect.disabled = True
            self.ids.press_save.disabled = True
            self.ids.toggle_record_play.disabled = False
            
            #change text color of REC button
            self.ids.toggle_record_play.color = (1,1,1,1)
            self.ids.toggle_record_play.text = 'PLAY'
            
            #set play position to 0 and set size of recorded sequence
            self.play_pos = 0
            self.tc_start = self.play_pos
            self.tc_count = 0
            self.current_count = 0
            self.ids.tc_slider.max = len(self.rec_array)
            self.ids.tc_slider.steps = len(self.rec_array)
            self.rec_size = int(str(len(self.rec_array)))
            self.bool_stop = True
            self.bool_record = False
            self.bool_play = False
            print("DATA LOADED")
            self.ids.press_save.disabled = False

        elif self.bool_record == False and self.bool_play == False and self.bool_stop == True:
            #Change color of play button
            self.ids.toggle_record_play.color = (0,1,0,1)
            #play sequence from registered play position
            Clock.schedule_interval(self.play_values, 1)
            #toggle record and play button
            self.bool_play = True
            self.bool_stop = False
        
        elif self.bool_record == False and self.bool_play == True and self.bool_stop == False:
            #stop playing and save play position
            Clock.unschedule(self.play_values)
            #change color of play button
            self.ids.toggle_record_play.color = (1,1,1,1)
            self.bool_play = False
            self.bool_stop = True
        

    def press_new(self):
        self.bool_connect = False
        self.bool_record = False
        self.ids.toggle_record_play.text = 'REC'
        self.ids.toggle_record_play.color = (1,1,1,1)
        self.ids.toggle_connect.disabled = False
        self.ids.toggle_connect.color = (1,1,1,1)
        self.bool_play = False
        self.bool_stop = False
        self.ids.press_save.disabled = True
        self.ids.toggle_record_play.disabled = True
        self.current_count = 0
        self.entries_count = 0
        self.ids.sent_packets.text = "RECEIVED PACKETS: " + str(self.entries_count)
        self.h = 0
        self.m = 0
        self.s = 0
        self.ids.tc.text = ('%02d:%02d:%02d' % (int(self.h), int(self.m), int(self.s)))
        self.tc_started = False
        self.tc_seconds = 0
        self.tc_count = 0
        self.play_pos = 0
        self.rec_array = []
        self.ids.tc_slider.max = len(self.rec_array)
        self.ids.tc_slider.steps = len(self.rec_array)
        self.rec_size = int(str(len(self.rec_array)))
        self.values_delta = []
        self.values_theta = []
        self.values_alphaL = []
        self.values_alphaH = []
        self.values_betaL = []
        self.values_betaH = []
        self.values_gammaL = []
        self.values_gammaH = []
        self.play_delta = []
        self.play_theta = []
        self.play_alphaL = []
        self.play_alphaH = []
        self.play_betaL = []
        self.play_betaH = []
        self.play_gammaL = []
        self.play_gammaH = []



    def play_tc(self):
        self.play_pos = int(self.ids.tc_slider.value)        
        if self.current_count < self.play_pos:
            self.play_pos += 1
            self.play_values(1)
        elif self.current_count > self.play_pos:
            self.play_pos -= 1
            self.play_values(1)   
                     
        
    def play_values(self, dt):       
        #play brain waves values
        if self.ids.delta.state == "down":
            self.values_delta = self.play_delta[self.play_pos:self.values_display+self.play_pos]
        if self.ids.theta.state == "down":
            self.values_theta = self.play_theta[self.play_pos:self.values_display+self.play_pos]
        if self.ids.alpha_low.state == "down":
            self.values_alphaL = self.play_alphaL[self.play_pos:self.values_display+self.play_pos]
        if self.ids.alpha_high.state == "down":
            self.values_alphaH = self.play_alphaH[self.play_pos:self.values_display+self.play_pos]
        if self.ids.beta_low.state == "down":
            self.values_betaL = self.play_betaL[self.play_pos:self.values_display+self.play_pos]
        if self.ids.beta_high.state == "down":
            self.values_betaH = self.play_betaH[self.play_pos:self.values_display+self.play_pos]    
        if self.ids.gamma_low.state == "down":
            self.values_gammaL = self.play_gammaL[self.play_pos:self.values_display+self.play_pos]
        if self.ids.gamma_high.state == "down":
            self.values_gammaH = self.play_gammaH[self.play_pos:self.values_display+self.play_pos]
            
        if self.ids.toggle_osc.state == "down":
            self.client.send_message("/mindstalker", self.rec_array[self.play_pos])
            #print("SEND OSC")
        
        #update time code
        self.tc_count = self.play_pos
        self.m, self.s = divmod(self.tc_count, 60)
        self.h, self.m = divmod(self.m, 60)
        self.ids.tc.text = ('%02d:%02d:%02d' % (int(self.h), int(self.m), int(self.s)))
        
        if self.bool_play == True and self.bool_stop == False:
            self.play_pos += 1
            self.ids.tc_slider.value = self.play_pos
        
        if self.play_pos >= self.rec_size:
            #change color of play button
            self.ids.toggle_record_play.color = (1,1,1,1)
            self.bool_play = False
            self.bool_stop = True
            self.play_pos = 0
            Clock.unschedule(self.play_values)
        if self.ids.max_scale:
            self.values_display = int(self.ids.max_scale.value)
        
        self.current_count = self.play_pos
        
        
    def update_values(self, dt):
        if self.current_count != self.entries_count:
            if self.rec_autosave == True:
                self.rec_array.append(self.output)
            #update Delta Wave values
            if self.ids.delta.state == "down":
                self.values_delta.append(self.output[3])
                self.values_delta = self.values_delta[-self.values_display:]
            #update Theta Wave values
            if self.ids.theta.state == "down":   
                self.values_theta.append(self.output[4])
                self.values_theta = self.values_theta[-self.values_display:]
            #update Alpha Low Wave values
            if self.ids.alpha_low.state == "down":
                self.values_alphaL.append(self.output[5])
                self.values_alphaL = self.values_alphaL[-self.values_display:]
            #update Alhpa High Wave values
            if self.ids.alpha_high.state == "down":
                self.values_alphaH.append(self.output[6])
                self.values_alphaH = self.values_alphaH[-self.values_display:]
            #update Beta Low Wave values
            if self.ids.beta_low.state == "down":
                self.values_betaL.append(self.output[7])
                self.values_betaL = self.values_betaL[-self.values_display:]
            #update Beta High Wave values
            if self.ids.beta_high.state == "down":
                self.values_betaH.append(self.output[8])
                self.values_betaH = self.values_betaH[-self.values_display:]
            #update Gamma Low Wave values
            if self.ids.gamma_low.state == "down":
                self.values_gammaL.append(self.output[9])
                self.values_gammaL = self.values_gammaL[-self.values_display:]
            #update Gamma High Wave values
            if self.ids.gamma_high.state == "down":
                self.values_gammaH.append(self.output[10])
                self.values_gammaH = self.values_gammaH[-self.values_display:]
            #sent osc message if button is active
            if self.ids.toggle_osc.state == "down":
                self.client.send_message("/mindstalker", self.output)
                #print("SEND OSC")
            if self.current_count != self.entries_count:
                #set current count to count of received data entries
                self.current_count = self.entries_count
            #send string to label that shows received packets
            self.ids.sent_packets.text = "SENT PACKETS: " + str(self.entries_count)
            self.ids.tc_slider.max = self.current_count
            self.ids.tc_slider.steps = self.current_count
            self.ids.tc_slider.value = self.current_count
            #update time code
            if self.bool_record == True:
                self.tc_count = self.current_count - self.tc_start
                self.m, self.s = divmod(self.tc_count, 60)
                self.h, self.m = divmod(self.m, 60)
                self.ids.tc.text = ('%02d:%02d:%02d' % (int(self.h), int(self.m), int(self.s)))
                #Control autosave counter
                self.asave_count += 1
                print(self.asave_count)
                if self.asave_count >= (self.asave_time * 60) and self.asave_bool == True:
                    self.asave_trig = True
                    self.asave_count = 0     
                #save autosave array to file, when trigger is True
                if self.asave_trig:
                    np.save('autosave/' + time.strftime("%Y%m%d-%H%M%S"), np.array(self.rec_array))
                    self.asave_trig = False 

        
class ColorDownButton(ToggleButton):
    """
    Button with a possibility to change the color on on_press (similar to background_down in normal Button widget)
    """
    background_color_normal = ListProperty([1, 1, 1, 0.4])
    background_color_down = ListProperty([1, 1, 1, 1])

    def __init__(self, **kwargs):
        super(ColorDownButton, self).__init__(**kwargs)
        self.background_normal = ""
        self.background_down = ""
        self.background_color = self.background_color_normal
        
        
    def on_state(self, widget, value):
        if value == 'down':
            self.background_color = self.background_color_down
        else:
            self.background_color = self.background_color_normal
            

class MindStalkerApp(App):
    
    def build(self):
        self.root = Viewport(size=(480,320))
        self.msgrid = MindStalkerGrid()
        self.msgrid.apply(self.msgrid.dev_path,
                          self.msgrid.asave_bool, 
                          self.msgrid.asave_time, 
                          self.msgrid.osc_bool, 
                          self.msgrid.osc_ip, 
                          self.msgrid.osc_port, 
                          self.msgrid.sc_bool, 
                          self.msgrid.jack_bool, 
                          self.msgrid.sc_path)
        self.root.add_widget(self.msgrid) 
          
        return self.root


Factory.register('LoadDialog', cls=LoadDialog)
Factory.register('SaveDialog', cls=SaveDialog)

if __name__ == '__main__':
    
    MindStalkerApp().run()




